let doc = {
    header: "header",
    body: "body",
    footer: "footer",
    data: "data",
    application: {
        application_header: {
            a_header: "header"
        },
        application_body: {
            a_body: "body"
        },
        application_footer: {
            a_footer: "footer"
        },
        application_data: {
            a_data: "data"
        }
        
    },
    fill: function() {
        this.header = prompt("Введите значение для свойства header объекта doc:");
        this.body = prompt("Введите значение для свойства body объекта doc:");
        this.footer = prompt("Введите значение для свойства footer объекта doc:");
        this.data = prompt("Введите значение для свойства data объекта doc:");
        this.application.application_header.a_header = prompt("Введите значение для свойства a_header объекта application:");
        this.application.application_body.a_body = prompt("Введите значение для свойства a_body объекта application:");
        this.application.application_footer.a_footer = prompt("Введите значение для свойства a_footer объекта application:");
        this.application.application_data.a_data = prompt("Введите значение для свойства a_data объекта application:");
    },
    show: function() {
        document.write(`<p>Doc header: ${this.header}`);
        document.write(`<p>Doc header: ${this.body}`);
        document.write(`<p>Doc header: ${this.footer}`);
        document.write(`<p>Doc header: ${this.data}`);
        document.write(`<p>Application header: ${this.application.application_header.a_header}`);
        document.write(`<p>Application header: ${this.application.application_body.a_body}`);
        document.write(`<p>Application header: ${this.application.application_footer.a_footer}`);
        document.write(`<p>Application header: ${this.application.application_data.a_data}`);
    }
};

doc.fill();
doc.show();

