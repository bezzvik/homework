window.addEventListener("load",  () => {

    // обработка события отправки запроса
    document.forms[0].addEventListener("submit", (e) => {
        let isValid = true;

        const login = document.getElementsByName("login")[0],
            password = document.getElementsByName("pass")[0];

        // если условие сработает значение в форме будет считаться не правильным.
        if (login.value.length == 0) {
            isValid = false;
            login.style.backgroundColor = "red";
            login.value = "Поле обязательно для заполнения";
        }
        if (password.value.length == 0) {
            isValid = false;
            password.style.backgroundColor = "red";
            password.type = "text";
            password.value = "Поле обязательно для заполнения";
        }

       
        if (!isValid) {
            e.preventDefault();
        }

        // авторизация для admin 12345
        let log = "admin"; //проверка на админ
        let psw = "12345";
        if(login.value == log && password.value == psw) {
           //const btn = getElementsByClassName("success");
           //btn.hidden = false;
          alert("Вы успешно авторизированы");
        }
    });

})